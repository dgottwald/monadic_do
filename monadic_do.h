#ifndef OPONN_MONADIC_DO_H
#define OPONN_MONADIC_DO_H

#include "monadic_do_impl.h"

namespace oponn {

//! This class represents a variable in a do-expression. The identity of the
//! variable is encoded in the template-parameter id.
//! Note that two instances with the same id represent the same variable. 
//! Note that this class has an unusual assignment operator that returns a
//! different class that represents the assigned value and the id of the 
//! variable.
template <int id>
class Var
{ 
public:
  Var() = default;
  
  template <typename F>
  detail::Var_Thunk<id, F> 
  operator=(F && f) const
  {
    return detail::Var_Thunk<id, F>(detail::make_thunk(std::forward<F>(f)));
  }
  
  template <typename F, typename ...IDs>
  detail::Var_Thunk<id, F, IDs...> 
  operator=(detail::Thunk<F, IDs...> const & thunk) const
  {
    return detail::Var_Thunk<id, F, IDs...>(thunk);
  }
  
  template <typename F, typename ...IDs>
  detail::Var_Thunk<id, F, IDs...>
  operator=(detail::Thunk<F, IDs...> && thunk) const
  {
    return detail::Var_Thunk<id, F, IDs...>(std::move(thunk));
  }
protected:
private:
};


//! The pipe-operator is used to connect callable entities with variables
//! whose values are extracted to call the entity later on.
template <typename F, int id>
detail::Thunk<F, detail::lvalue<id>> 
operator|(F && f, Var<id> const & )
{
  return detail::Thunk<F, detail::lvalue<id>>(std::forward<F>(f));
}

//! This function is selected in expressions like
//! Var<1234> /* const */ var;
//! foo | std::move(var);
//! regardless if var is const or not, hence the const && 
template <typename F, int id>
detail::Thunk<F, detail::rvalue<id>>
operator|(F && f, Var<id> const && )
{
  return detail::Thunk<F, detail::rvalue<id>>(std::forward<F>(f));
}

template <typename F, int id, typename ...IDs>
detail::Thunk<F, IDs..., detail::lvalue<id>> 
operator|(detail::Thunk<F, IDs...> const & thunk, Var<id> const & )
{
  return detail::Thunk<F, IDs..., detail::lvalue<id>>(thunk.f());
}

template <typename F, int id, typename ...IDs>
detail::Thunk<F, IDs..., detail::rvalue<id>>
operator|(detail::Thunk<F, IDs...> const & thunk, Var<id> const && )
{
  return detail::Thunk<F, IDs..., detail::rvalue<id>>(thunk.f());
}


//! The main function of this header tries to mimic the do-expression from
//! Haskell that is used as syntactic sugar to chain monadic calls using
//! the bind-function.
//! TODO: additional notes on monadic_bind, ...
//!
//! Example:
//! \code
//! #include "monadic_do.h"
//! #include "optional_monad.h"
//! 
//! using oponn::Var;
//! using oponn::monadic_do;
//! 
//! boost::optional<int> foo(int) { ... }
//! boost::optional<double> bar(int) { ... }
//! boost::optional<char> quux(int, double) { ... }
//! 
//! void example()
//! {
//!   Var<1> a;
//!   Var<2> b;
//!   
//!   boost::optional<char> c = monadic_do(a = []() { return foo(7); },
//!                                        b = bar | a,
//!                                        quux | a | b);
//!   
//! }
//! \endcode
template <typename ...Args>
decltype(auto) monadic_do(Args && ...args)
{
  return detail::do_impl(detail::Var_Stack<>(), std::forward<Args>(args)...);
}

} // namespace oponn

#endif // OPONN_MONADIC_DO_H
