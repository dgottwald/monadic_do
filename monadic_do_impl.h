#ifndef OPONN_MONADIC_DO_IMPL_H
#define OPONN_MONADIC_DO_IMPL_H

#include <utility>
#include <type_traits>
#include <cstdint>

namespace oponn {

namespace detail {

// *****************************************************************************
//
// Var_Instance
//
// *****************************************************************************

template <int id, typename T>
class Var_Instance
{
public:
  explicit Var_Instance(T const & value) : m_value(value) { }
  explicit Var_Instance(T && value) : m_value(std::move(value)) { }
  
  T & value() { return m_value; }
  T const & value() const { return m_value; }
protected:
private:
  T m_value;
};

template <int id, typename T>
Var_Instance<id, T> make_var_instance(T && t)
{
  return Var_Instance<id, T>(std::forward<T>(t));
}

// *****************************************************************************
//
// Var_Stack<Ts...>
//
// function                      | effect
// ------------------------------+----------------------------------------------
// Var_Stack<>()                 | creates an empty stack
// has_stack_id<id, Stack>       | Metafunction that is true if Stack contains a
//                               | Var_Instance with the given id
// id_index<id, Stack>           | Metafunction that returns the index of the
//                               | Var_Instance with given id
// push(Var_Instance &, Stack)   | pushes the reference of a new Var_Instance 
//   -> Stack                    | on the stack
// head<N>(Stack) -> T &         | returns the N-th head of the stack (i.e. the
//                               | element with index N)
// tail<N>(Stack) -> Stack       | returns the N-th tail of the stack
// erase<N>(Stack) -> Stack      | erases the N-th element of the stack and
//                               | returns a new stack without that element
// erase_id<id>(Stack) -> Stack  | erases the Var_Instance with given id from
//                               | the stack and returns a new stack without
//                               | that element
// value_id<id>(Stack) -> T &    | returns the value of the Var_Instance of the
//                               | stack with given id
//
// *****************************************************************************


template <typename ...Ts> class Var_Stack;

template <>
class Var_Stack<>
{
public:
  Var_Stack() = default;
protected:
private:
};

template <typename T, typename ...Ts>
class Var_Stack<T, Ts...>
{
  using Head = T;
  using Tail = Var_Stack<Ts...>;
public:
  Var_Stack(Head & value,
            Tail const & tail)
  : m_head(value), m_tail(tail)
  { }
   
  Head & head() const { return m_head; }
  
  Tail const & tail() const { return m_tail; }
  
protected:
private:
  Head & m_head;
  Tail m_tail;
};

// ***** has_stack_id *****
template <int id, typename Stack> struct has_stack_id;

template <int id>
struct has_stack_id<id, Var_Stack<>> 
  : std::false_type 
{ };

template <int id, typename T, typename ...Ts>
struct has_stack_id<id, Var_Stack<Var_Instance<id, T>, Ts...>>
  : std::true_type
{ };

template <int id, typename T, typename ...Ts>
struct has_stack_id<id, Var_Stack<T, Ts...>>
  : has_stack_id<id, Var_Stack<Ts...>>
{ }; 

// ***** id_index *****
template <size_t N, int id, typename Stack> struct id_index_impl;

template <size_t N, int id, typename T, typename ...Ts>
struct id_index_impl<N, id, Var_Stack<Var_Instance<id, T>, Ts...>>
  : std::integral_constant<size_t, N>
{ };

template <size_t N, int id, typename T, typename ...Ts>
struct id_index_impl<N, id, Var_Stack<T, Ts...>>
  : id_index_impl<N + 1, id, Var_Stack<Ts...>>
{ };

template <int id, typename Stack> 
struct id_index 
  : id_index_impl<0, id, Stack>
{
  static_assert(has_stack_id<id, Stack>::value,
                "The stack does not contain a value with the given id");
};

// ***** push *****
template <int id, typename T, typename ...Ts>
Var_Stack<Var_Instance<id, T>, Ts...> 
push(Var_Instance<id, T> & value, Var_Stack<Ts...> const & stack)
{
  static_assert(!has_stack_id<id, Var_Stack<Ts...>>::value,
                "there must not be duplicate ids");
  return { value, stack };
}

// ***** head<N> *****
template <typename T, typename ...Ts>
T & head_impl(Var_Stack<T, Ts...> const & stack, 
              std::integral_constant<int, 0> )
{
  return stack.head();
}

template <int N, typename ...Ts>
decltype(auto) head_impl(Var_Stack<Ts...> const & stack, 
                         std::integral_constant<int, N> )
{
  return head_impl(stack.tail(), std::integral_constant<int, N - 1>());
}


template <int N, typename ...Ts>
decltype(auto) head(Var_Stack<Ts...> const & stack)
{
  static_assert(N >= 0, "N must not be negative");
  return head_impl(stack, std::integral_constant<int, N>());
}

// ***** tail<N> *****
template <typename T, typename ...Ts>
Var_Stack<Ts...> const & tail_impl(Var_Stack<T, Ts...> const & stack, 
                                   std::integral_constant<int, 0> )
{
  return stack.tail();
}

template <int N, typename ...Ts>
decltype(auto) tail_impl(Var_Stack<Ts...> const & stack, 
                         std::integral_constant<int, N> )
{
  return tail_impl(stack.tail(), std::integral_constant<int, N - 1>());
}

//! \return the tail of the N-th element:
//! tail<0>(stack) -> stack.tail()
//! tail<1>(stack) -> stack.tail().tail()
//! etc.
template <int N, typename ...Ts>
decltype(auto) tail(Var_Stack<Ts...> const & stack)
{
  static_assert(N >= 0, "N must not be negative");
  return tail_impl(stack, std::integral_constant<int, N>());
}

// ***** erase<N> *****
template <typename ...Ts, typename ...Us>
auto erase_impl(Var_Stack<Ts...> const & orig,
                Var_Stack<Us...> const & result,
                std::integral_constant<int, 0> )
{
  return push(orig.head(), result);
}

template <typename ...Ts, typename ...Us>
Var_Stack<Us...> erase_impl(Var_Stack<Ts...> const & orig,
                            Var_Stack<Us...> const & result,
                            std::integral_constant<int, -1> )
{
  return result;
}

template <int N, typename ...Ts, typename ...Us>
auto erase_impl(Var_Stack<Ts...> const & orig,
                Var_Stack<Us...> const & result,
                std::integral_constant<int, N> )
{
  return erase_impl(orig, 
                    push(head<N>(orig), result),
                    std::integral_constant<int, N - 1>());
}

template <int N, typename ...Ts>
auto erase(Var_Stack<Ts...> const & stack)
{
  static_assert(N >= 0, "N must not be negative");
  return erase_impl(stack,
                            tail<N>(stack),
                            std::integral_constant<int, N - 1>());
}

// ***** erase_id<id> *****
template <int id, typename ...Ts>
auto erase_id(Var_Stack<Ts...> const & stack)
{
  static_assert(has_stack_id<id, Var_Stack<Ts...>>::value,
                "The stack does not have a value with the given id");
  return erase<id_index<id, Var_Stack<Ts...>>::value>(stack);

}

// ***** value_id<id> *****
template <int id, typename ...Ts>
decltype(auto) value_id(Var_Stack<Ts...> const & stack)
{
  static_assert(has_stack_id<id, Var_Stack<Ts...>>::value,
                "The stack does not have a value with the given id");
  return head<id_index<id, Var_Stack<Ts...>>::value>(stack).value();
}



// *****************************************************************************
// *
// * Thunk
// *
// *****************************************************************************

template <int id> struct lvalue;
template <int id> struct rvalue;
template <typename ...IDs> struct id_list { };

//! This class stores a function and the ids of the Var's that it shall be 
//! called with. Usually it is created by chaining oponn::operator|
//! @param F type of the function
//! @param IDs id-numbers plus l/r-value-ness of the Var's
template <typename F, typename ...IDs>
class Thunk
{
public:
  template <typename G>
  explicit Thunk(G && g) : m_f(std::forward<G>(g)) { }
  
  F const & f() const { return m_f; }
  F &       f()       { return m_f; }
  
protected:
private:
  F m_f;
};

//! constructor function for Thunk that deduces F
template <typename F>
Thunk<F> make_thunk(F && f)
{
  return Thunk<F>(std::forward<F>(f));
}

template <typename F>
decltype(auto) monadic_bind_adl(F && f)
{
  return f();
}

template <typename F, typename ...Args>
decltype(auto) monadic_bind_adl(F && f, Args && ...args)
{
  return monadic_bind(std::forward<F>(f), std::forward<Args>(args)...);
}

template <typename F, typename ...Ts, typename ...Args>
auto evaluate_thunk_impl(F const & f,
                         Var_Stack<Ts...> const & stack,
                         id_list<>,
                         Args && ...args)
{
  return monadic_bind_adl(f, std::forward<Args>(args)...);
}

template <typename F, typename ...Ts, int id, typename ...IDs, typename ...Args>
auto evaluate_thunk_impl(F const & f,
                         Var_Stack<Ts...> const & stack,
                         id_list<lvalue<id>, IDs...>,
                         Args && ...args)
{
  return evaluate_thunk_impl(f,
                             stack,
                             id_list<IDs...>{},
                             std::forward<Args>(args)...,
                             value_id<id>(stack));
}

template <typename F, typename ...Ts, int id, typename ...IDs, typename ...Args>
auto evaluate_thunk_impl(F const & f,
                         Var_Stack<Ts...> const & stack,
                         id_list<rvalue<id>, IDs...>,
                         Args && ...args)
{
  return evaluate_thunk_impl(f,
                             erase_id<id>(stack),
                             id_list<IDs...>{},
                             std::forward<Args>(args)...,
                             std::move(value_id<id>(stack)));
}

template <typename F, typename ...IDs, typename ...Ts>
auto evaluate_thunk(Thunk<F, IDs...> const & thunk,
                    Var_Stack<Ts...> const & stack)
{
  return evaluate_thunk_impl(thunk.f(), stack, id_list<IDs...>{});
}

// helper
template <typename ...Ts>
Var_Stack<Ts...> erase_all(Var_Stack<Ts...> const & stack,
                           id_list<> )
{
  return stack;
}

template <typename ...Ts, int id, typename ...IDs>
auto erase_all(Var_Stack<Ts...> const & stack,
               id_list<lvalue<id>, IDs...> )
{
  return erase_all(stack, id_list<IDs...>{});
}               

template <typename ...Ts, int id, typename ...IDs>
auto erase_all(Var_Stack<Ts...> const & stack,
               id_list<rvalue<id>, IDs...> )
{
  return erase_all(erase_id<id>(stack), id_list<IDs...>{});
}               

// *****************************************************************************
// *
// * Var_Thunk
// *
// *****************************************************************************

template <int id, typename F, typename ...IDs>
class Var_Thunk
{
public:
  explicit Var_Thunk(Thunk<F, IDs...> const & thunk) : m_thunk(thunk) { }
  explicit Var_Thunk(Thunk<F, IDs...> && thunk) : m_thunk(std::move(thunk)) { }
  
  template <typename ...Ts>
  auto evaluate(Var_Stack<Ts...> const & stack) const
  {
    return make_var_instance<id>(evaluate_thunk(m_thunk, stack));
  }
protected:
private:
  Thunk<F, IDs...> m_thunk;
};

// *****************************************************************************
// *
// * do_impl
// *
// *****************************************************************************

template <typename ...Ts>
void do_impl(Var_Stack<Ts...> const & )
{ 
  // end of recursion -> do nothing
}

template <typename ...Ts, typename F, typename ...IDs>
decltype(auto) do_impl(Var_Stack<Ts...> const & stack,
                       Thunk<F, IDs...> const & thunk)
{
  // end of recursion -> return result of the function call
  return evaluate_thunk(thunk, stack);
}

template <typename ...Ts, typename F, typename ...IDs, typename ...Args>
decltype(auto) do_impl(Var_Stack<Ts...> const & stack,
                       Thunk<F, IDs...> const & thunk,
                       Args && ...args)
{
  // a normal thunk: evaluate it but discard the return value
  evaluate_thunk(thunk, stack);
  return do_impl(erase_all(stack, id_list<IDs...>{}), // remove all ids from the 
                                                      // stack that were moved
                                                      // from
                 std::forward<Args>(args)...);
}

template <typename ...Ts, int id, typename F, typename ...IDs, typename ...Args>
decltype(auto) do_impl(Var_Stack<Ts...> const & stack,
                       Var_Thunk<id, F, IDs...> const & thunk,
                       Args && ...args)
{
  // a Var_Thunk: store the result as local variable and put a reference to it
  // on the stack
  auto result = thunk.evaluate(stack);
  return do_impl(push(result, erase_all(stack, id_list<IDs...>{})),
                 std::forward<Args>(args)...);
}

} } // namespace oponn::detail

#endif // OPONN_MONADIC_DO_IMPL_H
