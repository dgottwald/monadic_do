#ifndef OPONN_OPTIONAL_MONAD_H
#define OPONN_OPTIONAL_MONAD_H

#include "boost/optional.hpp"

namespace xyz { 

template <typename T>
class Optional
{
public:
  Optional() = default;

  template <typename U>
  explicit Optional(U && value) : m_value(std::forward<U>(value)) { }
  
  bool is_initialized() const { return m_value.is_initialized(); }

  T const & get() const { return m_value.get(); }  
protected:
private:
  boost::optional<T> m_value;
};

template <typename F, typename T>
auto monadic_bind(F && f, Optional<T> const & arg)
  -> decltype(f(std::declval<T>()))
{
  if (arg.is_initialized())
  {
    return f(arg.get());
  }
  else
  {
    return {};
  }
}

// TODO:
// template <typename F, typename ...Ts>
// auto monadic_bind(F && f, boost::optional<Ts> const & ...args)

} // namespace oponn

#endif // OPONN_OPTIONAL_MONAD_H
